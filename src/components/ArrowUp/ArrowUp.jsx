import React from 'react'
import arrow from '../../img/arrow.png'
import './ArrowUp.css'

export const ArrowUp = () => {
  function scrollToTop() {
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }

  return (
    <div className='arrow' onClick={() => scrollToTop()}>
      <img src={arrow} alt='' />
    </div>
  )
}
