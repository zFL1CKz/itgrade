import React from 'react'

import './Footer.css'

export const Footer = () => {
  return (
    <div>
      <div className='footer__wrapper'>
        <div className='footer__copy'>2022© OpenDota</div>
        <div className='footer__nav'>
          <a href='https://blog.opendota.com/2014/08/01/faq/' target='_blank'>
            О нас
          </a>
          <a
            href='https://blog.opendota.com/2014/08/01/faq/#what-is-your-privacy-policy'
            target='_blank'>
            Политика конфидициальности
          </a>
          <a href='https://docs.opendota.com' target='_blank'>
            Документация API
          </a>
        </div>
      </div>
    </div>
  )
}
