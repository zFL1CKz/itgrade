import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { Modal } from '../Modal/Modal'

import logo from '../../img/logo.svg'
import arrow from '../../img/leftArrow.svg'

import classes from '../../modules/Form.module.css'
import './Header.css'

export const Header = () => {
  let [showMenu, setShowMenu] = useState(false)
  let [modalActive, setModalActive] = useState(false)
  let [currentModalScreen, setCurrentModalScreen] = useState('auth')
  const [checkbox, setCheckbox] = useState(false)

  useEffect(() => {
    if (modalActive === false) {
      setTimeout(() => {
        setCurrentModalScreen('auth')
      }, 200)
    }
  }, [modalActive])

  return (
    <div className='body__wrapper'>
      <header className='header' onClick={() => setShowMenu(!showMenu)}>
        <div className='header__menu'>
          <span></span>
          <span></span>
          <span></span>
        </div>
        <div className='header__logo'>
          <img src={logo} alt='' />
        </div>
      </header>

      <div className={showMenu ? 'menu active' : 'menu'}>
        <div className='menu__body'>
          <div className='menu__close' onClick={() => setShowMenu(!showMenu)}>
            <svg
              width='18'
              height='18'
              viewBox='0 0 18 18'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'>
              <path
                fill-rule='evenodd'
                clip-rule='evenodd'
                d='M0.292893 0.292893C0.683417 -0.0976311 1.31658 -0.0976311 1.70711 0.292893L9 7.58579L16.2929 0.292893C16.6834 -0.0976311 17.3166 -0.0976311 17.7071 0.292893C18.0976 0.683417 18.0976 1.31658 17.7071 1.70711L10.4142 9L17.7071 16.2929C18.0976 16.6834 18.0976 17.3166 17.7071 17.7071C17.3166 18.0976 16.6834 18.0976 16.2929 17.7071L9 10.4142L1.70711 17.7071C1.31658 18.0976 0.683417 18.0976 0.292893 17.7071C-0.0976311 17.3166 -0.0976311 16.6834 0.292893 16.2929L7.58579 9L0.292893 1.70711C-0.0976311 1.31658 -0.0976311 0.683417 0.292893 0.292893Z'
                fill='black'
              />
            </svg>
          </div>
          <div className='menu__content'>
            <div className='menu__title'>
              <img src={logo} alt='' />
            </div>
            <div className='menu__nav'>
              <Link to='/teamslist' className='menu__nav_link'>
                Список команд
                <br />
              </Link>
              <Link to='/matcheslist' className='menu__nav_link'>
                Список матчей
                <br />
              </Link>
              <div
                className='menu__name_link auth'
                onClick={() => {
                  setModalActive(true)
                  setShowMenu(!showMenu)
                }}>
                <svg
                  width='16'
                  height='20'
                  viewBox='0 0 16 20'
                  fill='none'
                  xmlns='http://www.w3.org/2000/svg'>
                  <path
                    fill-rule='evenodd'
                    clip-rule='evenodd'
                    d='M15.8414 16.138L15.074 13.5799C14.473 11.5764 12.4033 10.3992 10.3741 10.9065C8.81537 11.2962 7.18469 11.2962 5.62596 10.9065C3.59672 10.3992 1.5271 11.5764 0.926055 13.5799L0.15864 16.138C-0.418811 18.0628 1.02253 20 3.03212 20H12.9679C14.9775 20 16.4189 18.0628 15.8414 16.138ZM2.84171 14.1546L2.07429 16.7127C1.88181 17.3543 2.36226 18 3.03212 18H12.9679C13.6378 18 14.1182 17.3543 13.9258 16.7127L13.1583 14.1546C12.8643 13.1745 11.8519 12.5986 10.8592 12.8468C8.98196 13.3161 7.01809 13.3161 5.14089 12.8468C4.14819 12.5986 3.13574 13.1745 2.84171 14.1546Z'
                    fill='white'
                  />
                  <path
                    fill-rule='evenodd'
                    clip-rule='evenodd'
                    d='M13 5C13 7.76142 10.7614 10 8.00003 10C5.2386 10 3.00003 7.76142 3.00003 5C3.00003 2.23858 5.2386 0 8.00003 0C10.7614 0 13 2.23858 13 5ZM8.00003 8C9.65688 8 11 6.65685 11 5C11 3.34315 9.65688 2 8.00003 2C6.34317 2 5.00003 3.34315 5.00003 5C5.00003 6.65685 6.34317 8 8.00003 8Z'
                    fill='white'
                  />
                </svg>

                <div>Авторизация</div>
              </div>
            </div>
          </div>
        </div>
        <div
          className='menu__overlay'
          onClick={() => setShowMenu(!showMenu)}></div>
      </div>
      <Modal active={modalActive} setActive={setModalActive}>
        {currentModalScreen === 'auth' && (
          <div className='auth'>
            <form className={classes.auth__form}>
              <fieldset>
                <legend className={classes.auth__title}>Авторизация</legend>

                <label htmlFor='email'>Логин / E-mail</label>
                <input type='email' id='email' />

                <label htmlFor='pass'>Пароль</label>
                <input type='password' id='pass' />

                <button type='submit' className={classes.auth__btn}>
                  Войти
                </button>
              </fieldset>
            </form>

            <div className={classes.auth__link}>
              <div className={classes.auth__text}>Нет учетной записи?</div>
              <div
                className={classes.auth__reg}
                onClick={() => setCurrentModalScreen('reg')}>
                <span>Регистрация</span>
                <img src={arrow} alt='' />
              </div>
            </div>

            <div className={classes.auth__link}>
              <div className={classes.auth__text}>Забыли пароль?</div>
              <div className={classes.auth__reg}>
                <span>Восстановить пароль</span>
                <img src={arrow} alt='' />
              </div>
            </div>
          </div>
        )}
        {currentModalScreen === 'reg' && (
          <div className='reg'>
            <form className={classes.auth__form}>
              <fieldset>
                <legend className={classes.auth__title}>Регистрация</legend>

                <div className={classes.reg__group}>
                  <div>
                    <label htmlFor='email'>E-mail</label>
                    <input type='email' id='email' />
                  </div>

                  <div>
                    <label htmlFor='login'>Логин</label>
                    <input type='text' id='login' />
                  </div>

                  <div>
                    <label htmlFor='pass'>Пароль</label>
                    <input type='email' id='pass' />
                  </div>

                  <div>
                    <label htmlFor='repeatPass'>Повторите пароль</label>
                    <input type='password' id='repeatPass' />
                  </div>
                </div>

                <div className={classes.reg__checkbox}>
                  <input
                    type='checkbox'
                    id='checkbox'
                    className={classes.custom__checkbox}></input>
                  <label
                    htmlFor='checkbox'
                    className={classes.checkbox__label}
                    onClick={() => setCheckbox(!checkbox)}>
                    Согласен с политикой конфидициальности
                  </label>
                </div>

                <button
                  type='submit'
                  disabled={!checkbox}
                  className={classes.reg__btn}>
                  Регистрация
                </button>
              </fieldset>
            </form>
          </div>
        )}
      </Modal>
    </div>
  )
}
