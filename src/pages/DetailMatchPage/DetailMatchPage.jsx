import React, { useCallback, useEffect, useState } from 'react'
import axios from 'axios'
import { useLocation } from 'react-router-dom'

import { Loader } from '../../components/Loader/Loader'
import { Header } from '../../components/Header/Header'
import { Footer } from '../../components/Footer/Footer'
import { ArrowUp } from '../../components/ArrowUp/ArrowUp'

import warningImg from '../../img/details/warning.svg'
import analyzeImg from '../../img/details/analyze.svg'
import replayImg from '../../img/details/replay.svg'

import classes from '../../modules/Table.module.css'
import './DetailMatchPage.css'

export const DetailMatchPage = () => {
  const location = useLocation()
  const [details, setDetails] = useState([])
  const [isReady, setIsReady] = useState(false)
  const [analyzeWarning, setAnalyzeWarning] = useState(false)

  const [gameModeName, setGameModeName] = useState()
  const apiUrlMatch = `https://api.opendota.com/api/matches/${location.state}`
  const apiGetGameMode =
    'https://raw.githubusercontent.com/odota/dotaconstants/master/json/game_mode.json'

  const getDetails = useCallback(async () => {
    const response = await axios(apiUrlMatch)
    setDetails(response.data)

    if (Object.keys(details).length > 0) {
      await axios(apiGetGameMode).then((res) => {
        var gameMode = res.data[details.game_mode].name
          .split('_')
          .slice(2)
          .join(' ')
        setGameModeName(gameMode)
      })
      setIsReady(true)
    }
  }, [details.length, gameModeName])

  function checkZero(num) {
    if (num === 0) return '-'
    else return num
  }

  const checkTime = (time) => {
    let currentTime = Math.round(new Date().getTime() / 1000)
    let result = Math.floor((currentTime - time) / 60 - 27)
    if (result < 60) {
      let lastSymbol = parseInt(
        String(result).split('')[String(result).length - 1]
      )
      if (lastSymbol === 1 && result !== 11)
        return parseInt(result) + ' минуту назад'
      else if (
        (lastSymbol === 2 && result !== 12) ||
        (lastSymbol === 3 && result !== 13) ||
        (lastSymbol === 4 && result !== 14)
      )
        return parseInt(result) + ' минуты назад'
      else return parseInt(result) + ' минут назад'
    } else if (result / 60 < 24) {
      let lastSymbol = parseInt(
        String(parseInt(result / 60)).split('')[
          String(parseInt(result / 60)).length - 1
        ]
      )
      if (lastSymbol === 1 && result !== 11)
        return parseInt(result / 60) + ' час назад'
      else if (
        (lastSymbol === 2 && result !== 12) ||
        (lastSymbol === 3 && result !== 13) ||
        (lastSymbol === 4 && result !== 14)
      )
        return parseInt(result / 60) + ' часа назад'
      else return parseInt(result / 60) + ' часов назад'
    } else {
      let daysResult = parseInt(result / 60 - 23)
      let lastSymbol = parseInt(
        String(parseInt(daysResult)).split('')[
          String(parseInt(daysResult)).length - 1
        ]
      )
      if (lastSymbol === 1 && daysResult !== 11)
        return parseInt(daysResult) + ' день назад'
      else if (
        (lastSymbol === 2 && daysResult !== 12) ||
        (lastSymbol === 3 && daysResult !== 13) ||
        (lastSymbol === 4 && daysResult !== 14)
      )
        return parseInt(daysResult) + ' дня назад'
      else return parseInt(daysResult) + ' дней назад'
    }
  }

  function setItemUsage(obj) {
    let arr = Object.keys(obj).slice(0, 5)
    let result = []
    for (const item of arr) {
      result.push(item.split('_').join(' '))
    }
    return result
  }

  function checkLength(str) {
    if (str.split('').length > 13)
      return str.split('').slice(0, 10).join('') + '...'
    return str
  }

  function changeTime(dur) {
    let durMinutes = Math.floor(dur / 60)
    let durSeconds = dur % 60
    if (String(durSeconds).split('').length === 1) {
      if (durSeconds >= 6) return durMinutes + 1 + ':00'
      else return durMinutes + ':' + durSeconds + '0'
    } else return durMinutes + ':' + durSeconds
  }

  useEffect(() => {
    getDetails()
  }, [getDetails])

  if (!isReady) {
    return (
      <div>
        <div className='container'>
          <Header />
        </div>
        <Loader />
      </div>
    )
  } else {
    return (
      <div className='body__wrapper'>
        <div className='container'>
          <Header />

          <div className='content__wrapper'>
            <div className='main__title details__main_title'>
              Детальная информация матча
            </div>

            <div className='details'>
              <div className='details__content'>
                <div className='details__win'>
                  <img
                    src={
                      details.radiant_win
                        ? details.radiant_team.logo_url
                        : details.dire_team.logo_url
                    }
                    alt=''
                    className='details__win_img'
                  />
                  <span>
                    Победа:&nbsp;
                    {details.radiant_win
                      ? details.radiant_team.name
                      : details.dire_team.name}
                  </span>
                </div>
                <div className='details__stats'>
                  <div
                    className={`details__score ${
                      details.radiant_win ? 'green' : 'orange'
                    }`}>
                    {details.radiant_score}
                  </div>
                  <div className='details__stats_info'>
                    <div className='details__gamemode'>{gameModeName}</div>
                    <div className='details__duration'>
                      {changeTime(details.duration)}
                    </div>
                    <div className='details__endTime'>
                      Завершен&nbsp;{checkTime(details.start_time)}
                    </div>
                  </div>
                  <div
                    className={`details__score ${
                      !details.radiant_win ? 'green' : 'orange'
                    }`}>
                    {details.dire_score}
                  </div>
                </div>
                <div className='details__info'>
                  <div className='details__info_item'>
                    <div className='details__info_title'>ID матча</div>
                    <div className='details__info_text'>{details.match_id}</div>
                  </div>
                  <div className='details__info_item'>
                    <div className='details__info_title'>Регион</div>
                    <div className='details__info_text'>{details.region}</div>
                  </div>
                  <div className='details__info_item'>
                    <div className='details__info_title'>Навык</div>
                    <div className='details__info_text'>
                      {details.skill === null ? 'без навыка' : details.skill}
                    </div>
                  </div>
                </div>
              </div>

              {analyzeWarning && (
                <div className='details__warning'>
                  <img src={warningImg} alt='' />
                  <span>
                    Запись этого матча не может быть проанализирована, так как
                    недоступна вся информация о матче
                  </span>
                </div>
              )}

              <div className='details__btns'>
                <button
                  className='details__btn'
                  onClick={() => setAnalyzeWarning(true)}>
                  <img src={analyzeImg} alt='' />
                  <span>Анализ</span>
                </button>
                <a
                  href={details.replay_url}
                  download={`replay ${details.match_id}`}
                  className='details__btn'>
                  <img src={replayImg} alt='' />
                  <span>Загрузить запись</span>
                </a>
              </div>
            </div>

            <table className={`${classes.table} details__table`}>
              <thead>
                <tr>
                  <caption className='caption'>
                    <span className='caption__team'>
                      {details.radiant_team.name}
                    </span>
                    <span className='caption__stats'>статистика команды</span>
                    <span
                      className={`caption__status ${
                        details.radiant_win ? 'green' : 'orange'
                      }`}>
                      {details.radiant_win ? 'Победители' : 'Проигравшие'}
                    </span>
                  </caption>
                </tr>

                <tr className={classes.table__title}>
                  <th>Игрок</th>
                  <th>LVL</th>
                  <th className='green'>K</th>
                  <th className='orange'>D</th>
                  <th>A</th>
                  <th>LH/DN</th>
                  <th className='yellow'>NET</th>
                  <th>GPM/XPM</th>
                  <th>HD</th>
                  <th>TD</th>
                  <th>HH</th>
                  <th>Предметы / Бафы</th>
                </tr>
              </thead>

              <tbody>
                {details.players.slice(0, 5).map((item, index) => {
                  return (
                    <>
                      <tr key={index} className='details__tr'>
                        <td className='green'>
                          {checkLength(item.personaname)}
                        </td>
                        <td>{checkZero(item.level)}</td>
                        <td className='green'>{checkZero(item.kills)}</td>
                        <td className='orange'>{checkZero(item.deaths)}</td>
                        <td>{checkZero(item.assists)}</td>
                        <td>{`${checkZero(item.last_hits)} / ${checkZero(
                          item.denies
                        )}`}</td>
                        <td className='yellow'>{checkZero(item.net_worth)}</td>
                        <td>{`${checkZero(item.gold_per_min)} / ${
                          item.xp_per_min
                        }`}</td>
                        <td>{checkZero(item.hero_damage)}</td>
                        <td>{checkZero(item.tower_damage)}</td>
                        <td>{checkZero(item.hero_healing)}</td>
                        <td>
                          {setItemUsage(item.item_usage).map((itemUsage) => (
                            <div className='details__item_usage'>
                              {itemUsage}
                            </div>
                          ))}
                        </td>
                      </tr>
                    </>
                  )
                })}
              </tbody>
            </table>

            <table className={`${classes.table} details__table`}>
              <thead>
                <caption className='caption'>
                  <span className='caption__team'>
                    {details.dire_team.name}
                  </span>
                  <span className='caption__stats'>статистика команды</span>
                  <span
                    className={`caption__status ${
                      !details.radiant_win ? 'green' : 'orange'
                    }`}>
                    {!details.radiant_win ? 'Победители' : 'Проигравшие'}
                  </span>
                </caption>

                <tr className={classes.table__title}>
                  <th>Игрок</th>
                  <th>LVL</th>
                  <th className='green'>K</th>
                  <th className='orange'>D</th>
                  <th>A</th>
                  <th>LH/DN</th>
                  <th className='yellow'>NET</th>
                  <th>GPM/XPM</th>
                  <th>HD</th>
                  <th>TD</th>
                  <th>HH</th>
                  <th>Предметы / Бафы</th>
                </tr>
              </thead>
              <tbody>
                {details.players.slice(5, 10).map((item, index) => {
                  return (
                    <>
                      <tr key={index} className='details__tr'>
                        <td className='green'>
                          {checkLength(item.personaname)}
                        </td>
                        <td>{checkZero(item.level)}</td>
                        <td className='green'>{checkZero(item.kills)}</td>
                        <td className='orange'>{checkZero(item.deaths)}</td>
                        <td>{checkZero(item.assists)}</td>
                        <td>{`${checkZero(item.last_hits)} / ${checkZero(
                          item.denies
                        )}`}</td>
                        <td className='yellow'>{checkZero(item.net_worth)}</td>
                        <td>{`${checkZero(item.gold_per_min)} / ${
                          item.xp_per_min
                        }`}</td>
                        <td>{checkZero(item.hero_damage)}</td>
                        <td>{checkZero(item.tower_damage)}</td>
                        <td>{checkZero(item.hero_healing)}</td>
                        <td>
                          {setItemUsage(item.item_usage).map((itemUsage) => (
                            <div className='details__item_usage'>
                              {itemUsage}
                            </div>
                          ))}
                        </td>
                      </tr>
                    </>
                  )
                })}
              </tbody>
            </table>
          </div>
        </div>
        <Footer />
        <ArrowUp />
      </div>
    )
  }
}
