import React, { useCallback, useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import axios from 'axios'

import info from '../../img/info.svg'
import { Loader } from '../../components/Loader/Loader'
import { Header } from '../../components/Header/Header'
import { Footer } from '../../components/Footer/Footer'

import classes from '../../modules/Table.module.css'
import './MatchesList.css'
import { ArrowUp } from '../../components/ArrowUp/ArrowUp'

export const MatchesList = () => {
  const history = useHistory()
  const [matches, setMatches] = useState([])
  const [heroesImgs, setHeroesImgs] = useState([])
  const [isReady, setIsReady] = useState(false)

  let [currentLimit, setCurrentLimit] = useState(10)
  let [btnDisabled, setBtnDisabled] = useState(false)

  const apiUrlMatches = 'https://api.opendota.com/api/proMatches'

  const getMatches = useCallback(async () => {
    const response = await axios(apiUrlMatches)
    setMatches(response.data)
    if (matches.length > 0) {
      setIsReady(true)
    }
  }, [matches.length])

  const checkTime = (time) => {
    if (matches.length > 0) {
      let currentTime = Math.round(new Date().getTime() / 1000)
      let result = Math.floor((currentTime - time) / 60 - 27)
      if (result < 60) {
        let lastSymbol = parseInt(
          String(result).split('')[String(result).length - 1]
        )
        if (lastSymbol === 1 && result !== 11)
          return parseInt(result) + ' минуту назад'
        else if (
          (lastSymbol === 2 && result !== 12) ||
          (lastSymbol === 3 && result !== 13) ||
          (lastSymbol === 4 && result !== 14)
        )
          return parseInt(result) + ' минуты назад'
        else return parseInt(result) + ' минут назад'
      } else if (result / 60 < 24) {
        let lastSymbol = parseInt(
          String(parseInt(result / 60)).split('')[
            String(parseInt(result / 60)).length - 1
          ]
        )
        if (lastSymbol === 1 && result !== 11)
          return parseInt(result / 60) + ' час назад'
        else if (
          (lastSymbol === 2 && result !== 12) ||
          (lastSymbol === 3 && result !== 13) ||
          (lastSymbol === 4 && result !== 14)
        )
          return parseInt(result / 60) + ' часа назад'
        else return parseInt(result / 60) + ' часов назад'
      } else {
        let daysResult = parseInt(result / 60 - 23)
        let lastSymbol = parseInt(
          String(parseInt(daysResult)).split('')[
            String(parseInt(daysResult)).length - 1
          ]
        )
        if (lastSymbol === 1 && daysResult !== 11)
          return parseInt(daysResult) + ' день назад'
        else if (
          (lastSymbol === 2 && daysResult !== 12) ||
          (lastSymbol === 3 && daysResult !== 13) ||
          (lastSymbol === 4 && daysResult !== 14)
        )
          return parseInt(daysResult) + ' дня назад'
        else return parseInt(daysResult) + ' дней назад'
      }
    }
  }

  function changeTime(dur) {
    let durMinutes = Math.floor(dur / 60)
    let durSeconds = dur % 60
    if (String(durSeconds).split('').length === 1) {
      if (durSeconds >= 6) return durMinutes + 1 + ':00'
      else return durMinutes + ':' + durSeconds + '0'
    } else return durMinutes + ':' + durSeconds
  }

  function detailPage(link) {
    history.push(`/detailmatch/${link}`, link)
  }

  function tableUpdate() {
    setIsReady(false)
    if (currentLimit === 90) {
      setCurrentLimit((currentLimit += 10))
      setBtnDisabled(true)
    } else {
      setCurrentLimit((currentLimit += 10))
    }
    setIsReady(true)
    setTimeout(() => {
      window.scrollTo(0, document.body.scrollHeight)
    }, 250)
  }

  useEffect(() => {
    getMatches()
    setInterval(() => {
      getMatches()
    }, 60000)
  }, [getMatches])

  if (!isReady) {
    return (
      <div>
        <div className='container'>
          <Header />
        </div>
        <Loader />
      </div>
    )
  } else {
    return (
      <div className='body__wrapper'>
        <div className='container'>
          <Header />

          <div className='content__wrapper'>
            <div className='main__title'>Список матчей</div>
            <table className={classes.table}>
              <thead>
                <tr className={classes.table__title}>
                  <th>ID</th>
                  <th>Длительность</th>
                  <th>Radiant</th>
                  <th>Dire</th>
                </tr>
              </thead>
              <tbody className='matcheslist'>
                {matches.slice(0, currentLimit).map((item, index) => {
                  return (
                    <>
                      <tr
                        key={index}
                        className='matches'
                        onClick={() => detailPage(item.match_id)}>
                        <td>
                          <div
                            className={`${classes.green} ${classes.newGreen}`}>
                            {item.match_id}
                          </div>
                          <div
                            className={`${classes.table__group} ${classes.table__newGroup}`}>
                            <span>{checkTime(item.start_time)}</span>
                            <span>&nbsp;/&nbsp;{item.league_name}</span>
                          </div>
                        </td>

                        <td>{changeTime(item.duration)}</td>

                        <td className={item.radiant_win ? 'green' : ''}>
                          {item.radiant_name}
                        </td>
                        <td className={!item.radiant_win ? 'green' : ''}>
                          {item.dire_name}
                        </td>
                      </tr>
                    </>
                  )
                })}
              </tbody>
            </table>

            <button
              className={classes.table__btn}
              onClick={() => tableUpdate()}
              disabled={btnDisabled}>
              {!btnDisabled ? 'Загрузить еще' : 'Уже все загружено'}
            </button>
          </div>
        </div>
        <Footer />
        <ArrowUp />
      </div>
    )
  }
}
