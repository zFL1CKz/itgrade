import React, { useCallback, useEffect, useState } from 'react'
import axios from 'axios'
import info from '../../img/info.svg'

import { Loader } from '../../components/Loader/Loader'
import { Header } from '../../components/Header/Header'
import { Footer } from '../../components/Footer/Footer'

import classes from '../../modules/Table.module.css'
import './TeamsList.css'
import { ArrowUp } from '../../components/ArrowUp/ArrowUp'

export const TeamsList = () => {
  const [matches, setMatches] = useState([])
  const [isReady, setIsReady] = useState(false)
  const [topRating, setTopRating] = useState(0)
  const [topWins, setTopWins] = useState(0)
  const [topLoses, setTopLoses] = useState(0)

  let [currentLimit, setCurrentLimit] = useState(10)
  let [btnDisabled, setBtnDisabled] = useState(false)

  const apiUrlMatches = 'https://api.opendota.com/api/teams'

  const getMatches = async () => {
    const response = await axios(apiUrlMatches)
    setMatches(response.data.slice(0, 100))
    if (matches.length > 0) {
      setTopRating(matches[0].rating)
      checkStats()
      setIsReady(true)
    }
  }

  function changeRatingColor(el) {
    if (el > topRating - 50) return '#7540f0'
    else if (el > topRating - 300) return '#BBDD00'
    else if (el > topRating - 1000) return '#EECD00'
    else return '#FA7000'
  }

  function changeWinsColor(el) {
    if (el > topWins - 100) return '#7540f0'
    else if (el > topWins - 300) return '#BBDD00'
    else if (el > topWins - 700) return '#EECD00'
    else return '#FA7000'
  }

  function changeLosesColor(el) {
    if (el > topLoses - 100) return '#7540f0'
    else if (el > topLoses - 300) return '#BBDD00'
    else if (el > topLoses - 650) return '#EECD00'
    else return '#FA7000'
  }

  function checkIndex(index) {
    let lastSymbol = parseInt(String(index).split('')[String(index).length - 1])
    if (lastSymbol === 1 && index !== 11) return parseInt(index) + 'st'
    else if (lastSymbol === 2 && index !== 12) return parseInt(index) + 'nd'
    else if (lastSymbol === 3 && index !== 13) return parseInt(index) + 'rd'
    else return index + 'th'
  }

  const checkTime = (teamTime) => {
    if (matches.length > 0) {
      let currentTime = Math.round(new Date().getTime() / 1000.0)
      let result = Math.floor((currentTime - teamTime) / 60 / 60 / 24)
      if (result === 0) return 'Сегодня'
      else if (result === 1) return 'День назад'
      else if (
        result === 2 ||
        result === 3 ||
        result === 4 ||
        result === 22 ||
        result === 23 ||
        result === 24
      )
        return result + ' дня назад'
      else if (result === 21) return result + ' день назад'
      else if (result < 30) return result + ' дней назад'
      else if (result === 30 || result === 31) return 'Месяц назад'
      else return 'Более месяца назад'
    }
  }

  function checkStats() {
    let winsArr = []
    let losesArr = []
    for (let i = 0; i < matches.length; i++) {
      winsArr.push(matches[i].wins)
      losesArr.push(matches[i].losses)
    }
    const maxWin = Math.max.apply(null, winsArr)
    const maxLose = Math.max.apply(null, losesArr)
    setTopWins(maxWin)
    setTopLoses(maxLose)
  }

  function tableUpdate() {
    setIsReady(false)
    if (currentLimit === 90) {
      setCurrentLimit((currentLimit += 10))
      setBtnDisabled(true)
    } else {
      setCurrentLimit((currentLimit += 10))
    }
    setIsReady(true)
    setTimeout(() => {
      window.scrollTo(0, document.body.scrollHeight)
    }, 250)
  }

  useEffect(() => {
    getMatches()
  }, [matches.length])

  if (!isReady) {
    return (
      <div>
        <div className='container'>
          <Header />
        </div>
        <Loader />
      </div>
    )
  } else {
    return (
      <div className='body__wrapper'>
        <div className='container'>
          <Header />

          <div className='content__wrapper'>
            <div className='main__title'>Список команд</div>
            <table className={classes.table}>
              <thead>
                <tr className={classes.table__title}>
                  <th>Ранг</th>
                  <th>Название команды</th>
                  <th>Рейтинг</th>
                  <th>Победы</th>
                  <th>Проигрыши</th>
                </tr>
              </thead>
              <tbody>
                {matches.slice(0, currentLimit).map((item, index) => {
                  return (
                    <>
                      <tr key={index} className='teams'>
                        <td>{checkIndex(index + 1)}</td>
                        <td>
                          <div className={classes.table__group}>
                            <img src={item.logo_url} alt='' />
                            <div>
                              <div className={classes.green}>{item.name}</div>
                              <div>{checkTime(item.last_match_time)}</div>
                            </div>
                          </div>
                        </td>
                        <td>
                          {parseInt(item.rating)}
                          <div className={classes.table__line}>
                            <div
                              className={classes.table__line_width}
                              style={{
                                width: `${(item.rating * 100) / topRating}%`,
                                background: changeRatingColor(item.rating),
                              }}></div>
                          </div>
                        </td>

                        <td>
                          {item.wins}
                          <div className={classes.table__line}>
                            <div
                              className={classes.table__line_width}
                              style={{
                                width: `${(item.wins * 100) / topWins}%`,
                                background: changeWinsColor(item.wins),
                              }}></div>
                          </div>
                        </td>

                        <td>
                          {item.losses}
                          <div className={classes.table__line}>
                            <div
                              className={classes.table__line_width}
                              style={{
                                width: `${(item.losses * 100) / topLoses}%`,
                                background: changeLosesColor(item.losses),
                              }}></div>
                          </div>
                        </td>
                      </tr>
                    </>
                  )
                })}
              </tbody>

              <tfoot>
                <tr>
                  <td colSpan='5'>
                    <div className={classes.caption}>
                      <img src={info} alt='' />
                      <div>
                        Team Elo Rankings<span>k=32, init=1000</span>
                      </div>
                    </div>
                  </td>
                </tr>
              </tfoot>
            </table>

            <button
              className={classes.table__btn}
              onClick={() => tableUpdate()}
              disabled={btnDisabled}>
              {!btnDisabled ? 'Загрузить еще' : 'Уже все загружено'}
            </button>
          </div>
        </div>
        <Footer />
        <ArrowUp />
      </div>
    )
  }
}
