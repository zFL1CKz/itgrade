import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import { DetailMatchPage } from './pages/DetailMatchPage/DetailMatchPage'
import { MatchesList } from './pages/MatchesList/MatchesList'
import { TeamsList } from './pages/TeamsList/TeamsList'

export const useRoutes = () => {
  return (
    <Switch>
      <Route path='/teamslist' exact>
        <TeamsList />
      </Route>
      <Route path='/matcheslist' exact>
        <MatchesList />
      </Route>
      <Route path='/detailmatch/:id'>
        <DetailMatchPage />
      </Route>
      <Redirect to='/teamslist' />
    </Switch>
  )
}
